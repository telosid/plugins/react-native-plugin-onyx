# @telosid/react-native-onyx-plugin

Plugin for ONYX SDK for react-native

# Telos ID NPM Registry
```bash
echo @telosid:registry=https://gitlab.com/api/v4/projects/29462567/packages/npm/ >> .npmrc
```

## Installation

```sh
npm install @telosid/react-native-plugin-onyx
```

Add the OnyxCamera specs repo to your CocoaPods installation
```shell
pod repo add gitlab-telosid-plugins https://gitlab.com/telosid/plugins/specs.git
```

## [API](https://gitlab.com/telosid/plugins/onyx-typedefs#api)

## [Sample Project](https://gitlab.com/telosid/samples/onyx-react-native-example)

#### IOS
```bash
npm install -g ios-deploy
cd example/ios
pod install --repo-update
cd ..
react-native run-ios --device "${DEVICE_NAME}’s iPhone"
```

