import * as React from 'react';
import {
  Button,
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';

// You can create a .env file to hold the LICENSE_KEY
// @ts-ignore
import { LICENSE_KEY } from '@env';
import type {
  IOnyxConfiguration,
  IOnyxPluginResult,
  IOnyxResult,
} from '@telosid/onyx-typedefs';
import {
  FingerprintTemplateType,
  OnyxPluginAction,
  OnyxReticleOrientation,
} from '@telosid/onyx-typedefs';
import { Onyx } from '@telosid/react-native-plugin-onyx';

const onyxConfig: IOnyxConfiguration = {
  action: OnyxPluginAction.CAPTURE,
  licenseKey: LICENSE_KEY,
  returnRawImage: true,
  returnProcessedImage: true,
  returnFingerprintTemplate: FingerprintTemplateType.NONE,
  returnWSQ: true,
  returnSlapWSQ: true,
  manualCaptureText: 'Tap to manual capture',
  captureFingersText: 'Cap Fings',
  captureThumbText: 'Cap Thumb',
  fingersNotInFocusText: 'Fings Not focused',
  thumbNotInFocusText: 'thumb Not focused',
  computeNfiqMetrics: true,
  fingerDetectionTimeout: 60,
  reticleOrientation: OnyxReticleOrientation.LEFT,
};

export default function App() {
  const [result, setResult] = React.useState<IOnyxResult[] | undefined>();
  const launchOnyx = async () => {
    console.log('Onyx license: ' + LICENSE_KEY);
    try {
      const onyxPluginResult: IOnyxPluginResult = await Onyx.exec(onyxConfig);
      console.log('result = ', onyxPluginResult);
      setResult(onyxPluginResult.onyxResults);
    } catch (err) {
      console.error('OnyxError: ' + err);
    }
  };

  return result ? (
    <SafeAreaView style={styles.safeAreaContainer}>
      <ScrollView>
        {result.map((onyxResult: IOnyxResult, i: number) => {
          return (
            <View
              key={`onyxResult${i}`}
              style={{ padding: 10, alignItems: 'center' }}
            >
              <Text>{`Fingerprint ${i + 1}`}</Text>
              <Image
                source={{ uri: onyxResult.processedFingerprintDataUri }}
                style={styles.imageContainer}
              />
              <Text>
                {`NFIQ: ${
                  onyxResult.captureMetrics?.nfiqMetrics?.nfiqScore ||
                  'CaptureMetrics not available'
                }`}
              </Text>
            </View>
          );
        })}
      </ScrollView>
    </SafeAreaView>
  ) : (
    <View style={styles.container}>
      <Button onPress={launchOnyx} title="Launch ONYX" />
    </View>
  );
}

// Get device width
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  safeAreaContainer: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
  imageContainer: {
    height: deviceWidth * 0.8,
    width: deviceWidth * 0.8,
  },
  image: {
    flex: 1,
    height: undefined,
    width: undefined,
  },
});
