package com.telosid.onyx.plugin.reactnative;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.ReadableNativeMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;

import timber.log.Timber;

public class OnyxPlugin extends ReactContextBaseJavaModule {

  public static final String TAG = "OnyxPlugin";
  public static final String IMAGE_URI_PREFIX = "data:image/png;base64,";
  public static JSONObject mArgs;
  private static String mExecuteAction;
  public static OnyxPluginAction mOnyxPluginAction;


  public OnyxPlugin(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  public enum OnyxPluginAction {
    CAPTURE("capture"),
    MATCH("match");
    private final String key;

    OnyxPluginAction(String key) {
      this.key = key;
    }

    public String getKey() {
      return this.key;
    }
  }

  public enum OnyxConfig {
    ACTION("action"),
    LICENSE_KEY("licenseKey"),
    RETURN_RAW_IMAGE("returnRawImage"),
    RETURN_PROCESSED_IMAGE("returnProcessedImage"),
    RETURN_ENHANCED_IMAGE("returnEnhancedImage"),
    RETURN_SLAP_IMAGE("returnSlapImage"),
    RETURN_SLAP_WSQ("returnSlapWSQ"),
    SHOULD_BINARIZE_PROCESSED_IMAGE("shouldBinarizeProcessedImage"),
    RETURN_FULL_FRAME_IMAGE("returnFullFrameImage"),
    FULL_FRAME_MAX_IMAGE_HEIGHT("fullFrameMaxImageHeight"),
    RETURN_WSQ("returnWSQ"),
    RETURN_FINGERPRINT_TEMPLATE("returnFingerprintTemplate"),
    CROP_SIZE("cropSize"),
    CROP_SIZE_WIDTH("width"),
    CROP_SIZE_HEIGHT("height"),
    CROP_FACTOR("cropFactor"),
    SHOW_LOADING_SPINNER("showLoadingSpinner"),
    USE_MANUAL_CAPTURE("useManualCapture"),
    MANUAL_CAPTURE_TEXT("manualCaptureText"),
    CAPTURE_FINGERS_TEXT("captureFingersText"),
    CAPTURE_THUMB_TEXT("captureThumbText"),
    FINGERS_NOT_IN_FOCUS_TEXT("fingersNotInFocusText"),
    THUMB_NOT_IN_FOCUS_TEXT("thumbNotInFocusText"),
    USE_ONYX_LIVE("useOnyxLive"),
    USE_FLASH("useFlash"),
    RETICLE_ORIENTATION("reticleOrientation"),
    COMPUTE_NFIQ_METRICS("computeNfiqMetrics"),
    TARGET_PIXELS_PER_INCH("targetPixelsPerInch"),
    SUBJECT_ID("subjectId"),
    UPLOAD_METRICS("uploadMetrics"),
    RETURN_ONYX_ERROR_ON_LOW_QUALITY("returnOnyxErrorOnLowQuality"),
    CAPTURE_QUALITY_THRESHOLD("captureQualityThreshold"),
    FINGER_DETECTION_TIMEOUT("fingerDetectionTimeout"),
    RETICLE_ORIENTATION_LEFT("LEFT"),
    RETICLE_ORIENTATION_RIGHT("RIGHT"),
    RETICLE_ORIENTATION_THUMB_PORTRAIT("THUMB_PORTRAIT"),
    FINGERPRINT_TEMPLATE_TYPE_NONE("NONE"),
    FINGERPRINT_TEMPLATE_TYPE_INNOVATRICS("INNOVATRICS"),
    FINGERPRINT_TEMPLATE_TYPE_ISO("ISO"),
    REFERENCE("reference"),
    PROBE("probe"),
    PYRAMID_SCALES("pyramidScales");
    private final String key;

    OnyxConfig(String key) {
      this.key = key;
    }

    public String getKey() {
      return this.key;
    }
  }

  @Override
  public String getName() {
    return "OnyxPlugin";
  }

  private ReactApplicationContext reactContext;
  private static Promise promise;

  @ReactMethod
  public void exec(ReadableMap options, Promise promise) throws JSONException {
    Log.d("TAG", "options" + options.toString());
    HashMap<String, Object> hashMap = options.toHashMap();
    mArgs = new JSONObject(hashMap);
    OnyxPlugin.promise = promise;
    String action = mArgs.getString(OnyxConfig.ACTION.getKey());

    if (StringUtils.isEmpty(action)) {
      onError("Must provide an action to execute");
      return;
    }

    if (!mArgs.has(OnyxConfig.LICENSE_KEY.getKey()) ||
        StringUtils.isEmpty(mArgs.getString(OnyxConfig.LICENSE_KEY.getKey()))) {
      onError("Must provide an ONYX license key");
      return;
    }
    mExecuteAction = action;
    Timber.v("OnyxPlugin action: %s", mExecuteAction);

    if (mExecuteAction.equalsIgnoreCase(OnyxPluginAction.MATCH.getKey())) {
      mOnyxPluginAction = OnyxPluginAction.MATCH;
    } else if (mExecuteAction.equalsIgnoreCase(OnyxPluginAction.CAPTURE.getKey())) {
      mOnyxPluginAction = OnyxPluginAction.CAPTURE;
    }

    if (null != mOnyxPluginAction) {
      switch (mOnyxPluginAction) {
        case MATCH:
//          doMatch();
          onError("Not implemented in ReactNative, due to libc++ conflict with IDKit");
          break;
        case CAPTURE:
          launchOnyx();
          break;
      }
    } else {
      onError("Invalid plugin action");
    }
  }

  private void launchOnyx() {
    Intent onyxIntent = new Intent(reactContext, OnyxActivity.class);
    onyxIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    Objects.requireNonNull(reactContext.getCurrentActivity()).startActivity(onyxIntent);
  }

  public static void onFinished(int resultCode, JSONObject result) {
    if (resultCode == Activity.RESULT_OK) {
      Log.v("result: %s", result.toString());
      try {
        WritableMap map = OnyxPlugin.convertJsonToMap(result);
        map.putString(OnyxConfig.ACTION.getKey(), mExecuteAction);
        promise.resolve(map);
      } catch (JSONException e) {
        onError(e.getMessage());
      }
    } else if (resultCode == Activity.RESULT_CANCELED) {
      onError("Cancelled");
    }
  }

  public static void onError(String errorMessage) {
    Timber.e(errorMessage);
    promise.reject(errorMessage);
  }

  public static WritableMap convertJsonToMap(JSONObject jsonObject) throws JSONException {
    WritableMap map = new WritableNativeMap();

    Iterator<String> iterator = jsonObject.keys();
    while (iterator.hasNext()) {
      String key = iterator.next();
      Object value = jsonObject.get(key);
      if (value instanceof JSONObject) {
        map.putMap(key, convertJsonToMap((JSONObject) value));
      } else if (value instanceof JSONArray) {
        map.putArray(key, convertJsonToArray((JSONArray) value));
        if (("option_values").equals(key)) {
          map.putArray("options", convertJsonToArray((JSONArray) value));
        }
      } else if (value instanceof Boolean) {
        map.putBoolean(key, (Boolean) value);
      } else if (value instanceof Integer) {
        map.putInt(key, (Integer) value);
      } else if (value instanceof Double) {
        map.putDouble(key, (Double) value);
      } else if (value instanceof String) {
        map.putString(key, (String) value);
      } else {
        map.putString(key, value.toString());
      }
    }
    return map;
  }

  public static WritableArray convertJsonToArray(JSONArray jsonArray) throws JSONException {
    WritableArray array = new WritableNativeArray();

    for (int i = 0; i < jsonArray.length(); i++) {
      Object value = jsonArray.get(i);
      if (value instanceof JSONObject) {
        array.pushMap(convertJsonToMap((JSONObject) value));
      } else if (value instanceof JSONArray) {
        array.pushArray(convertJsonToArray((JSONArray) value));
      } else if (value instanceof Boolean) {
        array.pushBoolean((Boolean) value);
      } else if (value instanceof Integer) {
        array.pushInt((Integer) value);
      } else if (value instanceof Double) {
        array.pushDouble((Double) value);
      } else if (value instanceof String) {
        array.pushString((String) value);
      } else {
        array.pushString(value.toString());
      }
    }
    return array;
  }

  public static JSONObject convertMapToJson(ReadableMap readableMap) throws JSONException {
    JSONObject object = new JSONObject();
    ReadableMapKeySetIterator iterator = readableMap.keySetIterator();
    while (iterator.hasNextKey()) {
      String key = iterator.nextKey();
      switch (readableMap.getType(key)) {
        case Null:
          object.put(key, JSONObject.NULL);
          break;
        case Boolean:
          object.put(key, readableMap.getBoolean(key));
          break;
        case Number:
          object.put(key, readableMap.getDouble(key));
          break;
        case String:
          object.put(key, readableMap.getString(key));
          break;
        case Map:
          object.put(key, convertMapToJson(Objects.requireNonNull(readableMap.getMap(key))));
          break;
        case Array:
          object.put(key, convertArrayToJson(Objects.requireNonNull(readableMap.getArray(key))));
          break;
      }
    }
    return object;
  }

  public static JSONArray convertArrayToJson(ReadableArray readableArray) throws JSONException {

    JSONArray array = new JSONArray();

    for (int i = 0; i < readableArray.size(); i++) {
      switch (readableArray.getType(i)) {
        case Null:
          break;
        case Boolean:
          array.put(readableArray.getBoolean(i));
          break;
        case Number:
          array.put(readableArray.getDouble(i));
          break;
        case String:
          array.put(readableArray.getString(i));
          break;
        case Map:
          array.put(convertMapToJson(readableArray.getMap(i)));
          break;
        case Array:
          array.put(convertArrayToJson(readableArray.getArray(i)));
          break;
      }
    }
    return array;
  }

}
