package com.telosid.onyx.plugin.reactnative;

import static com.dft.onyxcamera.config.OnyxConfiguration.ErrorCallback.Error.AUTOFOCUS_FAILURE;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.widget.LinearLayout;

import com.dft.onyx.FingerprintTemplate;
import com.dft.onyx.NfiqMetrics;
import com.dft.onyxcamera.config.Onyx;
import com.dft.onyxcamera.config.OnyxConfiguration;
import com.dft.onyxcamera.config.OnyxConfigurationBuilder;
import com.dft.onyxcamera.config.OnyxError;
import com.dft.onyxcamera.ui.CaptureMetrics;
import com.dft.onyxcamera.ui.reticles.Reticle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class OnyxActivity extends Activity {
  private static final String TAG = OnyxActivity.class.getSimpleName();
  public static final String SLAP_IMAGE_DATA_URI = "slapImageDataUri";

  public static final String BASE_64_ENCODED_SLAP_WSQ_BYTES = "base64EncodedSlapWsqBytes";

  public static final String FULL_FRAME_IMAGE_DATA_URI = "fullFrameImageDataUri";
  public static final String CAPTURE_METRICS = "captureMetrics";
  public static final String NFIQ_METRICS = "nfiqMetrics";
  public static final String NFIQ_SCORE = "nfiqScore";
  public static final String LIVENESS_CONFIDENCE = "livenessConfidence";
  public static final String QUALITY_METRIC = "qualityMetric";
  public static final String BASE_64_ENCODED_FINGERPRINT_TEMPLATE = "base64EncodedFingerprintTemplate";
  public static final String BASE_64_ENCODED_WSQ_BYTES = "base64EncodedWsqBytes";
  public static final String ENHANCED_FINGERPRINT_DATA_URI = "enhancedFingerprintDataUri";
  public static final String PROCESSED_FINGERPRINT_DATA_URI = "processedFingerprintDataUri";
  public static final String RAW_FINGERPRINT_DATA_URI = "rawFingerprintDataUri";
  private Activity mActivity;
  private Context mContext;
  private Onyx mOnyx;
  private boolean mUseManualCapture = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mActivity = this;
    mContext = this;
    requestWindowFeature(Window.FEATURE_NO_TITLE);

    LinearLayout rootView = new LinearLayout(mContext);
    rootView.setOrientation(LinearLayout.VERTICAL);
    LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
    );
    rootView.setLayoutParams(llp);

    setContentView(rootView);
    setupOnyx();
  }

  @Override
  protected void onResume() {
    super.onResume();
  }

  private void setupUI() {

  }

  private void setupOnyx() {
    try {
      // Create an OnyxConfigurationBuilder and configure it with desired options
      String onyxLicense = OnyxPlugin.mArgs.getString(
              OnyxPlugin.OnyxConfig.LICENSE_KEY.getKey());

      OnyxConfigurationBuilder onyxConfigurationBuilder = new OnyxConfigurationBuilder()
              .configActivity(mActivity)
              .licenseKey(onyxLicense)
              .successCallback(onyxSuccessCallback)
              .errorCallback(error -> {
                Log.e(TAG, error.toString());
                if (error.error != AUTOFOCUS_FAILURE) {
                  OnyxPlugin.onError(error.errorMessage);
                  finish();
                } else {
                  mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                      if (!mUseManualCapture) {
                        mOnyx.capture();
                      }
                    }
                  });
                }
              })
              .onyxCallback(configuredOnyx -> mActivity.runOnUiThread(new Runnable() {
                public void run() {
                  mOnyx = configuredOnyx;
                  mOnyx.create(mActivity);
                }
              }));

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_RAW_IMAGE.getKey())) {
        onyxConfigurationBuilder.returnRawImage(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_RAW_IMAGE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_PROCESSED_IMAGE.getKey())) {
        onyxConfigurationBuilder.returnProcessedImage(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_PROCESSED_IMAGE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_ENHANCED_IMAGE.getKey())) {
        onyxConfigurationBuilder.returnEnhancedImage(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_ENHANCED_IMAGE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_SLAP_IMAGE.getKey())) {
        onyxConfigurationBuilder.returnSlapImage(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_SLAP_IMAGE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_SLAP_WSQ.getKey())) {
        onyxConfigurationBuilder.returnSlapWsqData(OnyxPlugin.mArgs.getBoolean(
          OnyxPlugin.OnyxConfig.RETURN_SLAP_WSQ.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.SHOULD_BINARIZE_PROCESSED_IMAGE.getKey())) {
        onyxConfigurationBuilder.shouldBinarizeProcessedImage(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.SHOULD_BINARIZE_PROCESSED_IMAGE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_FULL_FRAME_IMAGE.getKey())) {
        onyxConfigurationBuilder.returnFullFrameImage(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_FULL_FRAME_IMAGE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.FULL_FRAME_MAX_IMAGE_HEIGHT.getKey())) {
        onyxConfigurationBuilder.fullFrameMaxImageHeight((float) OnyxPlugin.mArgs.getDouble(
                OnyxPlugin.OnyxConfig.FULL_FRAME_MAX_IMAGE_HEIGHT.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_WSQ.getKey())) {
        onyxConfigurationBuilder.returnWSQ(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_WSQ.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_FINGERPRINT_TEMPLATE.getKey())) {
        String fingerprintTemplateTypeString = OnyxPlugin.mArgs.getString(
                OnyxPlugin.OnyxConfig.RETURN_FINGERPRINT_TEMPLATE.getKey());
        if (!fingerprintTemplateTypeString.isEmpty()) {
          OnyxConfiguration.FingerprintTemplateType fingerprintTemplateType = OnyxConfiguration.FingerprintTemplateType.NONE;
          if (fingerprintTemplateTypeString.equalsIgnoreCase(
                  OnyxPlugin.OnyxConfig.FINGERPRINT_TEMPLATE_TYPE_NONE.getKey())) {
            fingerprintTemplateType = OnyxConfiguration.FingerprintTemplateType.NONE;
          } else if (fingerprintTemplateTypeString.equalsIgnoreCase(
                  OnyxPlugin.OnyxConfig.FINGERPRINT_TEMPLATE_TYPE_INNOVATRICS.getKey())) {
            fingerprintTemplateType = OnyxConfiguration.FingerprintTemplateType.INNOVATRICS;
          } else if (fingerprintTemplateTypeString.equalsIgnoreCase(
                  OnyxPlugin.OnyxConfig.FINGERPRINT_TEMPLATE_TYPE_ISO.getKey())) {
            fingerprintTemplateType = OnyxConfiguration.FingerprintTemplateType.ISO;
          }
          onyxConfigurationBuilder.returnFingerprintTemplate(fingerprintTemplateType);
        }
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.CROP_SIZE.getKey())) {
        JSONObject cropSize = OnyxPlugin.mArgs.getJSONObject(
                OnyxPlugin.OnyxConfig.CROP_SIZE.getKey());
        int width = 512;
        int height = 300;
        if (cropSize.has(OnyxPlugin.OnyxConfig.CROP_SIZE_WIDTH.getKey())) {
          String cropSizeWidthString = cropSize.getString(
                  OnyxPlugin.OnyxConfig.CROP_SIZE_WIDTH.getKey());
          if (!cropSizeWidthString.isEmpty()) {
            width = Integer.parseInt(cropSizeWidthString);
          }
        }
        if (cropSize.has(OnyxPlugin.OnyxConfig.CROP_SIZE_HEIGHT.getKey())) {
          String cropSizeHeightString = cropSize.getString(
                  OnyxPlugin.OnyxConfig.CROP_SIZE_HEIGHT.getKey());
          if (!cropSizeHeightString.isEmpty()) {
            height = Integer.parseInt(cropSizeHeightString);
          }
        }
        onyxConfigurationBuilder.cropSize(width, height);
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.CROP_FACTOR.getKey())) {
        String cropFactorString = OnyxPlugin.mArgs.getString(
                OnyxPlugin.OnyxConfig.CROP_FACTOR.getKey());
        if (!cropFactorString.isEmpty()) {
          onyxConfigurationBuilder.cropFactor(Double.parseDouble(cropFactorString));
        }
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.SHOW_LOADING_SPINNER.getKey())) {
        onyxConfigurationBuilder.showLoadingSpinner(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.SHOW_LOADING_SPINNER.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.USE_MANUAL_CAPTURE.getKey())) {
        mUseManualCapture = OnyxPlugin.mArgs.getBoolean(OnyxPlugin.OnyxConfig.USE_MANUAL_CAPTURE.getKey());
        onyxConfigurationBuilder.useManualCapture(mUseManualCapture);
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.MANUAL_CAPTURE_TEXT.getKey())) {
        onyxConfigurationBuilder.manualCaptureText(OnyxPlugin.mArgs.getString(
                OnyxPlugin.OnyxConfig.MANUAL_CAPTURE_TEXT.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.CAPTURE_FINGERS_TEXT.getKey())) {
        onyxConfigurationBuilder.captureFingersText(OnyxPlugin.mArgs.getString(
          OnyxPlugin.OnyxConfig.CAPTURE_FINGERS_TEXT.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.CAPTURE_THUMB_TEXT.getKey())) {
        onyxConfigurationBuilder.captureThumbText(OnyxPlugin.mArgs.getString(
          OnyxPlugin.OnyxConfig.CAPTURE_THUMB_TEXT.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.FINGERS_NOT_IN_FOCUS_TEXT.getKey())) {
        onyxConfigurationBuilder.fingersNotInFocusText(OnyxPlugin.mArgs.getString(
          OnyxPlugin.OnyxConfig.FINGERS_NOT_IN_FOCUS_TEXT.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.THUMB_NOT_IN_FOCUS_TEXT.getKey())) {
        onyxConfigurationBuilder.thumbNotInFocusText(OnyxPlugin.mArgs.getString(
          OnyxPlugin.OnyxConfig.THUMB_NOT_IN_FOCUS_TEXT.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.USE_ONYX_LIVE.getKey())) {
        onyxConfigurationBuilder.useOnyxLive(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.USE_ONYX_LIVE.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.USE_FLASH.getKey())) {
        onyxConfigurationBuilder.useFlash(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.USE_FLASH.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETICLE_ORIENTATION.getKey())) {
        String reticleOrientationString = OnyxPlugin.mArgs.getString(
                OnyxPlugin.OnyxConfig.RETICLE_ORIENTATION.getKey());
        if (!reticleOrientationString.isEmpty()) {
          Reticle.Orientation reticleOrientation = Reticle.Orientation.LEFT;
          if (reticleOrientationString.equalsIgnoreCase(
                  OnyxPlugin.OnyxConfig.RETICLE_ORIENTATION_LEFT.getKey())) {
            reticleOrientation = Reticle.Orientation.LEFT;
          } else if (reticleOrientationString.equalsIgnoreCase(
                  OnyxPlugin.OnyxConfig.RETICLE_ORIENTATION_RIGHT.getKey())) {
            reticleOrientation = Reticle.Orientation.RIGHT;
          } else if (reticleOrientationString.equalsIgnoreCase(
                  OnyxPlugin.OnyxConfig.RETICLE_ORIENTATION_THUMB_PORTRAIT.getKey())) {
            reticleOrientation = Reticle.Orientation.THUMB_PORTRAIT;
          }
          onyxConfigurationBuilder.reticleOrientation(reticleOrientation);
        }
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.COMPUTE_NFIQ_METRICS.getKey())) {
        onyxConfigurationBuilder.computeNfiqMetrics(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.COMPUTE_NFIQ_METRICS.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.TARGET_PIXELS_PER_INCH.getKey())) {
        onyxConfigurationBuilder.targetPixelsPerInch(OnyxPlugin.mArgs.getDouble(
                OnyxPlugin.OnyxConfig.TARGET_PIXELS_PER_INCH.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.SUBJECT_ID.getKey())) {
        onyxConfigurationBuilder.subjectId(OnyxPlugin.mArgs.getString(
                OnyxPlugin.OnyxConfig.SUBJECT_ID.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.UPLOAD_METRICS.getKey())) {
        onyxConfigurationBuilder.uploadMetrics(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.UPLOAD_METRICS.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.RETURN_ONYX_ERROR_ON_LOW_QUALITY.getKey())) {
        onyxConfigurationBuilder.returnOnyxErrorOnLowQuality(OnyxPlugin.mArgs.getBoolean(
                OnyxPlugin.OnyxConfig.RETURN_ONYX_ERROR_ON_LOW_QUALITY.getKey()));
      }

      if (OnyxPlugin.mArgs.has(OnyxPlugin.OnyxConfig.CAPTURE_QUALITY_THRESHOLD.getKey())) {
        onyxConfigurationBuilder.captureQualityThreshold(OnyxPlugin.mArgs.getDouble(
                OnyxPlugin.OnyxConfig.CAPTURE_QUALITY_THRESHOLD.getKey()));
      }

      // Finally, build the OnyxConfiguration
      onyxConfigurationBuilder.buildOnyxConfiguration();
    } catch (JSONException e) {
      onError("JSONException: " + e.getMessage());
    }
  }

  private OnyxConfiguration.SuccessCallback onyxSuccessCallback = onyxResult -> {
    JSONObject result = new JSONObject();
    ArrayList<JSONObject> onyxResults = new ArrayList<>();
    ArrayList<Bitmap> rawFingerprintImages = null;
    ArrayList<Bitmap> processedFingerprintImages = null;
    ArrayList<Bitmap> enhancedFingerprintImages = null;
    Bitmap slapImage = null;
    byte[] slapWsqData = null;
    Bitmap fullFrameImage = null;
    ArrayList<byte[]> wsqDataArrayList = null;
    ArrayList<FingerprintTemplate> fingerprintTemplates = null;
    int numberFingersProcessed = 0;
    try {
      if (null != onyxResult.getRawFingerprintImages()) {
        rawFingerprintImages = onyxResult.getRawFingerprintImages();
        if (rawFingerprintImages.size() > numberFingersProcessed) {
          numberFingersProcessed = rawFingerprintImages.size();
        }
      }
      if (null != onyxResult.getProcessedFingerprintImages()) {
        processedFingerprintImages = onyxResult.getProcessedFingerprintImages();
        if (processedFingerprintImages.size() > numberFingersProcessed) {
          numberFingersProcessed = processedFingerprintImages.size();
        }
      }
      if (null != onyxResult.getEnhancedFingerprintImages()) {
        enhancedFingerprintImages = onyxResult.getEnhancedFingerprintImages();
        if (enhancedFingerprintImages.size() > numberFingersProcessed) {
          numberFingersProcessed = enhancedFingerprintImages.size();
        }
      }
      if (null != onyxResult.getSlapImage()) {
        slapImage = onyxResult.getSlapImage();
      }
      if (null != onyxResult.getSlapWsqData()) {
        slapWsqData = onyxResult.getSlapWsqData();
      }
      if (null != onyxResult.getFullFrameImage()) {
        fullFrameImage = onyxResult.getFullFrameImage();
      }
      if (null != onyxResult.getWsqData()) {
        wsqDataArrayList = onyxResult.getWsqData();
        if (wsqDataArrayList.size() > numberFingersProcessed) {
          numberFingersProcessed = wsqDataArrayList.size();
        }
      }
      if (null != onyxResult.getFingerprintTemplates()) {
        fingerprintTemplates = onyxResult.getFingerprintTemplates();
        if (fingerprintTemplates.size() > numberFingersProcessed) {
          numberFingersProcessed = fingerprintTemplates.size();
        }
      }

      Log.v(TAG, "numberFingersProcessed: " + numberFingersProcessed);
      for (int i = 0; i < numberFingersProcessed; i++) {
        JSONObject iOnyxResult = new JSONObject();
        JSONObject captureMetrics = new JSONObject();
        String rawFingerprintDataUri;
        String processedFingerprintDataUri;
        String enhancedFingerprintDataUri;
        String base64EncodedWsqBytes;
        String base64EncodedFingerprintTemplate = null;
        if (null != rawFingerprintImages &&
                rawFingerprintImages.size() == numberFingersProcessed) {
          rawFingerprintDataUri = getDataUriFromBitmap(
                  rawFingerprintImages.get(i));
          iOnyxResult.put(RAW_FINGERPRINT_DATA_URI, rawFingerprintDataUri);
        }
        if (null != processedFingerprintImages &&
                processedFingerprintImages.size() == numberFingersProcessed) {
          processedFingerprintDataUri = getDataUriFromBitmap(
                  processedFingerprintImages.get(i));
          iOnyxResult.put(PROCESSED_FINGERPRINT_DATA_URI, processedFingerprintDataUri);
        }
        if (null != enhancedFingerprintImages &&
                enhancedFingerprintImages.size() == numberFingersProcessed) {
          enhancedFingerprintDataUri = getDataUriFromBitmap(
                  enhancedFingerprintImages.get(i));
          iOnyxResult.put(ENHANCED_FINGERPRINT_DATA_URI, enhancedFingerprintDataUri);
        }
        if (null != wsqDataArrayList &&
                wsqDataArrayList.size() == numberFingersProcessed) {
          base64EncodedWsqBytes = Base64.encodeToString(wsqDataArrayList.get(i), Base64.NO_WRAP)
                  .trim();
          iOnyxResult.put(BASE_64_ENCODED_WSQ_BYTES, base64EncodedWsqBytes);
        }
        if (null != fingerprintTemplates &&
                fingerprintTemplates.size() == numberFingersProcessed) {
          base64EncodedFingerprintTemplate = Base64.encodeToString(
                          fingerprintTemplates.get(i).getData(), Base64.NO_WRAP)
                  .trim();
          iOnyxResult.put(BASE_64_ENCODED_FINGERPRINT_TEMPLATE,
                  base64EncodedFingerprintTemplate);
        }
        if (null != onyxResult.getMetrics()) {
          CaptureMetrics metrics = onyxResult.getMetrics();
          captureMetrics.put(QUALITY_METRIC, metrics.getQualityMetric());
          captureMetrics.put(LIVENESS_CONFIDENCE, metrics.getLivenessConfidence());
          JSONObject nfiqMetrics = new JSONObject();
          Log.v(TAG, "getNfiqMetrics().size(): " + metrics.getNfiqMetrics());

          if (null != metrics.getNfiqMetrics() &&
                  metrics.getNfiqMetrics().size() == numberFingersProcessed) {
            List<NfiqMetrics> nfiqMetricsList = metrics.getNfiqMetrics();
            nfiqMetrics.put(NFIQ_SCORE, nfiqMetricsList.get(i).getNfiqScore());
          }
          captureMetrics.put(NFIQ_METRICS, nfiqMetrics);
        }
        iOnyxResult.put(CAPTURE_METRICS, captureMetrics);
        onyxResults.add(iOnyxResult);
      }

      JSONObject iOnyxResult = new JSONObject();
      if (null != slapImage) {
        String slapImageDataUri = getDataUriFromBitmap(slapImage);
        if (onyxResults.size() != 0) {
          onyxResults.get(0).put(SLAP_IMAGE_DATA_URI, slapImageDataUri);
        } else {
          iOnyxResult.put(SLAP_IMAGE_DATA_URI, slapImageDataUri);
        }
      }
      if (null != slapWsqData) {
        String base64SlapWsqData = Base64.encodeToString(slapWsqData, Base64.NO_WRAP)
          .trim();
        if (onyxResults.size() != 0) {
          onyxResults.get(0).put(BASE_64_ENCODED_SLAP_WSQ_BYTES, base64SlapWsqData);
        } else {
          iOnyxResult.put(BASE_64_ENCODED_SLAP_WSQ_BYTES, base64SlapWsqData);
        }
      }
      if (null != fullFrameImage) {
        String fullFrameImageDataUri = getDataUriFromBitmap(
                fullFrameImage);
        if (onyxResults.size() != 0) {
          onyxResults.get(0).put(FULL_FRAME_IMAGE_DATA_URI, fullFrameImageDataUri);
        } else {
          iOnyxResult.put(FULL_FRAME_IMAGE_DATA_URI, fullFrameImageDataUri);
        }
      }
      if (onyxResults.size() == 0) {
        onyxResults.add(iOnyxResult);
      }
    } catch (JSONException e) {
      String errorMessage = "Failed to set JSON key value pair: " + e.getMessage();
      onError(errorMessage);
    }

    try {
      result.put("onyxResults", new JSONArray(onyxResults.toArray()));
    } catch (JSONException e) {
      String errorMessage = "Failed to create JSONArray from onyxResults: " + e.getMessage();
      onError(errorMessage);
    }

    OnyxPlugin.onFinished(Activity.RESULT_OK, result);
    finish();
  };

  private String getDataUriFromBitmap(Bitmap bitmap) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
    byte[] imageBytes = stream.toByteArray();
    bitmap.recycle();
    String dataUri;
    String encodedBytes = Base64.encodeToString(imageBytes, Base64.NO_WRAP).trim();
    dataUri = OnyxPlugin.IMAGE_URI_PREFIX + encodedBytes;
    return dataUri;
  }

  private void onError(String errorMessage) {
    OnyxPlugin.onError(errorMessage);
    finish();
  }
}
