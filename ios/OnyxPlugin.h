#import <React/RCTBridgeModule.h>
#import <OnyxCamera/OnyxConfigurationBuilder.h>
#import <OnyxCamera/Onyx.h>

@class OnyxResult;
@class OnyxError;
@class Onyx;

@interface OnyxPlugin : NSObject <RCTBridgeModule>
@property UIViewController* viewController;
@property NSString* onyxPluginAction;
@property NSString* executeAction;
@property NSDictionary* args;
@property RCTPromiseResolveBlock resolve;
@property RCTPromiseRejectBlock reject;
@property OnyxResult* onyxResult;
- (void) capture;
- (void(^)(OnyxResult* onyxResult))onyxSuccessCallback;
- (void(^)(OnyxError* onyxError)) onyxErrorCallback;
- (void(^)(Onyx* configuredOnyx))onyxCallback;
- (NSString*) getFingerprintImageUri:(UIImage*)image;
- (NSString*) getBase64EncodedString:(NSData*)data;
@end
