import { NativeModules, Platform } from 'react-native';
import type {
  IOnyxConfiguration,
  IOnyxPluginResult,
} from '@telosid/onyx-typedefs';

const LINKING_ERROR =
  `The package 'react-native-onyx-plugin' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo managed workflow\n';

const NativeOnyxPlugin = NativeModules.OnyxPlugin
  ? NativeModules.OnyxPlugin
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

export class Onyx {
  static exec(options: IOnyxConfiguration): Promise<IOnyxPluginResult> {
    return NativeOnyxPlugin.exec(options);
  }
}
